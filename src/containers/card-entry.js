import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "../components/button";
import { API_ENDPOINT_INFORMATION } from "../constants/api-endpoint-information";
import { getNonCriticalErrorById } from "../state/selectors/error";
import styled from "styled-components";
import { submitPayment } from "../state/thunks";
import { getResolvingStatus } from "../state/selectors/resolving";
import Resolution from "../constants/resolution";
import { getResponse } from "../state/selectors/response";

const Container = styled.div`
display: flex;
flex-direction: column;
min-width: 400px;
min-height: 0;
form {
  display: flex;
  flex-direction: column;
  min-width: 0px;
  min-height: 0;
}
input {
  box-sizing: border-box;
  min-width: 38px;
  min-height: 38px;
  border: 1px solid #cccccc;
  border-radius: 4px;
  opacity: .8;
  font-size: 14pt;
  padding: 0 3px;
}
.error {
  max-width: 400px;
  color: #ca1313;
}
.information {
  max-width: 400px;
  color: #228B22;
}
`;


const StyledButton = styled(Button)`
  margin-top: 0;
  margin-left: 0;
`;

class CardEntry extends Component {
  state = {
    cardNumber: "",
    expirationDate: "",
    zip: "",
    nameOnCard: "",
    cvv: "",
    errorMessage: ""
  }

  submitPayment = () => {
    const { submitPayment } = this.props;
    const { cardNumber, expirationDate, zip, nameOnCard, cvv } = this.state;

    if (!nameOnCard) {
      this.setState({ errorMessage: "Please enter a name" });
      return
    }

    if (!cardNumber) {
      this.setState({ errorMessage: "Please enter a card number" });
      return
    }

    if (!expirationDate) {
      this.setState({ errorMessage: "Please enter an expiration date" });
      return
    }

    if (!cvv) {
      this.setState({ errorMessage: "Please enter a CVV" });
      return
    }

    if (!zip) {
      this.setState({ errorMessage: "Please enter a zip code" });
      return
    }

    this.setState({ errorMessage: null });
    submitPayment(cardNumber, expirationDate, zip, nameOnCard, cvv);
  }

  render() {
    const { errorMessage } = this.state;
    const { error, resolvingSave, responseCode } = this.props;

    let errorString = error ? error.message : errorMessage;

    let responseText = getResponseText(responseCode);

    return (
      <Container>
        <form onKeyDown={this.checkForEnter}>
          <input
            type="text"
            placeholder="Name on card"
            maxLength={50}
            onChange={(event) => this.setState({ nameOnCard: event.target.value })}
            value={this.state.nameOnCard}
          />
          <br />
          <input
            type="text"
            placeholder="Card number"
            maxLength={20}
            onChange={(event) => this.setState({ cardNumber: event.target.value })}
            value={this.state.cardNumber}
          />
          <br />
          <input
            type="text"
            placeholder="MM/YY - Expiration Date"
            maxLength={4}
            onChange={(event) => this.setState({ expirationDate: event.target.value })}
            value={this.state.expirationDate}
          />
          <br />
          <input
            type="text"
            placeholder="CVV"
            maxLength={3}
            onChange={(event) => this.setState({ cvv: event.target.value })}
            value={this.state.cvv}
          />
          <br />
          <input
            type="text"
            placeholder="Zip code"
            maxLength={5}
            onChange={(event) => this.setState({ zip: event.target.value })}
            value={this.state.zip}
          />
          <br />
          <StyledButton
            text="Submit Payment"
            disabled={resolvingSave}
            onClick={() => {
              this.submitPayment();
            }}
          />
        </form>
        <br />
        <div className="error">
          {errorString}
        </div>
        <br />
        <div className="information">
          {responseText}
        </div>
      </Container>
    )
  }
}

const getResponseText = (responseCode) => {
  switch (responseCode) {
    case 0:
      return "The payment was processed successfully";
    case 30:
      return "There was an error with one of the entered fields";
    default:
      return null;
  }
};

const mapStateToProps = state => ({
  responseCode: getResponse(state),
  resolvingSave: getResolvingStatus(state, Resolution.SAVE),
  error: getNonCriticalErrorById(state, API_ENDPOINT_INFORMATION.PAYMENT.errorId)
})

const mapDispatchToProps = {
  submitPayment
}

export default connect(mapStateToProps, mapDispatchToProps)(CardEntry);