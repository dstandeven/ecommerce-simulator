import React, { Component } from "react";
import { connect } from "react-redux";
import {
  HashRouter,
  Route
} from "react-router-dom";
import asyncComponent from "../components/async-component";
import ECommercePNG from "../resources/e-commerce.png";
import styled from "styled-components";

const CardEntry = asyncComponent(() => import("./card-entry"));

const AppWrapper = styled.div`
  display: flex;
  height: 100vh;
  overflow: hidden;
`;

const AppContent = styled.div`
  display: flex;
  flex-grow: 1;
  flex-wrap: wrap;
  overflow-y: auto;
  padding: 25px;
`;

const SideNav = styled.div`
  background-color: #2860CC;
  min-width: 250px;
`;

const Logo = styled.div`
  display: flex;
  justify-content: center;
  padding: 1.5rem 0;
  img {
    display: block;
    max-width: 75px;
    max-height: 75px;
  }
`;

class AppRouter extends Component {
  render() {
    return (
      <AppWrapper>
        <HashRouter>
          <React.Fragment>
            <SideNav>
              <Logo>
                <img src={ECommercePNG} alt="logo" title="Logo image" />
              </Logo>
            </SideNav>
            <AppContent>
              <Route path="" component={CardEntry} />
            </AppContent>
          </React.Fragment>
        </HashRouter>
      </AppWrapper>
    )
  }
}

const mapStateToProps = state => ({
})

export default connect(mapStateToProps)(AppRouter);