export const HTTP_METHODS = {
  DELETE: "delete",
  GET: "get",
  POST: "post",
  PUT: "put"
};