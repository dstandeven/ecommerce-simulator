// Build endpoint information
export const API_ENDPOINT_INFORMATION = {
  PAYMENT: {
    urlPath: `${window.configuration.paymentEndpoint}/singularlogix/api/1/pinlessdebit`,
    errorId: "PAYMENT"
  }
};