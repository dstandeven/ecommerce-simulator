import React from 'react';
import { render } from 'react-dom';
import { Provider } from "react-redux";
import configureStore from "./configure-store";
import AppRouter from "./containers/app-router";
import StyleWrapper from "./configure-styles";
import "react-table/react-table.css";
import { initialize } from "./client-api";

// Get the React container
const root = document.getElementById("root");

// Configure the store
const store = configureStore();

// Initialize the API
initialize(window.configuration.paymentEndpoint)

// Create the application
const app = (
  <Provider store={store}>
    <StyleWrapper>
      <AppRouter />
    </StyleWrapper>
  </Provider>
);

render(app, root);