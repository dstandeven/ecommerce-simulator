import React from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";

// Class used to provide theming throughout the application
const applicationTheme  = {
  blue: "#2860CC"
}

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    box-sizing: border-box;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }
`

const GlobalStyles = ({ children }) => (
  <ThemeProvider theme={applicationTheme}>
    <div>
      <GlobalStyle />
      {children}
    </div>
  </ThemeProvider>
)

export default GlobalStyles;
