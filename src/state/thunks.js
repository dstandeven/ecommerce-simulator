import { API_ENDPOINT_INFORMATION } from "../constants/api-endpoint-information";
import { logNonCriticalError, clearNonCriticalError } from "./actions/creators/errors";
import { postPayment } from "../client-api/endpoints/payment";
import { setResolving } from "../state/actions/creators/resolving";
import Resolution from "../constants/resolution";
import { setResponse } from "./actions/creators/response";

export const submitPayment = (cardNumber, expirationDate, zip, nameOnCard, cvv) => async dispatch => {
  try {
    dispatch(setResolving(Resolution.SAVE, true));
    dispatch(setResponse(null));
    dispatch(clearNonCriticalError(API_ENDPOINT_INFORMATION.PAYMENT.errorId));

    const requestBody = {
      TerminalNumber: "EC001001", // A hypothetical online "terminal"
      SingularLogixKey: "262919BE-AED0-4FBE-85BA-1DE328F9A9F4", // This is a test app so no need yet for a real one
      FromAccountType: 3, // This is a test app so just send Checking
      MCCCode: 1, // This is a test app so just send 6010
      PANEntryMethod: 4, // This is an online transaction so send Cardless
      PurchaseAmount: 19.99, // Test those pennies
      SurchargeAmount: 2.87, // $2.87 isn't a bad surcharge
      SequenceNumber: generateSequenceNumber(999999), // "Random" numbers for sequence number
      CardNumber: cardNumber,
      ExpirationDate: expirationDate,
      Zip: zip,
      NameOnCard: nameOnCard,
      CVV: cvv,
      MerchantWebsite: "www.paypal.com" // Because why not?
    };

    const result = await postPayment(requestBody);
    dispatch(setResponse(result.responseCode));
  } catch (error) {
    dispatch(logNonCriticalError(API_ENDPOINT_INFORMATION.PAYMENT.errorId, undefined, error.message));
  } finally {
    dispatch(setResolving(Resolution.SAVE, false));
  }
}

const generateSequenceNumber = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
}