import { RESOLVING_SET } from "../constants/resolving"

export const setResolving = (resolution, resolving) => ({
  type: RESOLVING_SET,
  resolution: resolution,
  resolving: resolving
});