import * as errorActions from "../constants/errors";

export const logNonCriticalError = (originId, dispatchedAction, message, status) => ({
  type: errorActions.ERROR_LOG_NON_CRITICAL,
  originId,
  dispatchedAction,
  message,
  status
});

export const logCriticalError = (dispatchedAction, message) => ({
  type: errorActions.ERROR_LOG_CRITICAL,
  message,
  dispatchedAction
});

export const retryFailedAction = (id, error) => ({
  type: errorActions.ERROR_RETRY_FAILED_ACTION,
  error,
  id
});

export const clearNonCriticalError = (id) => ({
  type: errorActions.ERROR_CLEAR_NON_CRITICAL,
  id,
});