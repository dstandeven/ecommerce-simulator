import { RESPONSE_SET } from "../constants/response"

export const setResponse = (responseCode) => ({
  type: RESPONSE_SET,
  responseCode
});