export const getCriticalError = state => state.errorList.critical;

export const getNonCriticalErrorById = (state, id) => state.errorList[id];
