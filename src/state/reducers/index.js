import { combineReducers } from "redux";
import errorList from "./error-list";
import resolving from "./resolving";
import response from "./response";

// Combine each reducer
export default combineReducers({
  errorList,
  response,
  resolving
});
