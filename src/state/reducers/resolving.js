import Resolution from "../../constants/resolution";
import { RESOLVING_SET } from "../actions/constants/resolving";

export default (
  state = {
    [Resolution.SAVE]: false
  }, action) => {
  switch (action.type) {
    case RESOLVING_SET:
      return {
        ...state,
        [action.resolution]: action.resolving
      };
    default:
      return state;
  }
}