import { ERROR_CLEAR_NON_CRITICAL, ERROR_LOG_CRITICAL, ERROR_LOG_NON_CRITICAL } from "../actions/constants/errors";

export default (state = {}, action) => {
  switch (action.type) {
    case ERROR_LOG_CRITICAL:
      return {
        ...state,
        critical: {
          dispatchedAction: action.dispatchedAction,
          message: action.message,
        }
      };
    case ERROR_LOG_NON_CRITICAL:
      return {
        ...state,
        [action.originId]: {
          dispatchedAction: action.dispatchedAction,
          message: action.message,
          status: action.status
        }
      };
    case ERROR_CLEAR_NON_CRITICAL:
      return {
        ...state,
        [action.id]: undefined
      }
    default:
      return state;
  }
}