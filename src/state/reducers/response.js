import { RESPONSE_SET } from "../actions/constants/response";

export default (state = null, action) => {
  switch (action.type) {
    case RESPONSE_SET:
      return action.responseCode;
    default:
      return state;
  }
}