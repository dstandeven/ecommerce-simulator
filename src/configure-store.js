import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./state/reducers";
import thunk from 'redux-thunk';

export default function configureStore() {
  // Setup middlewares for Redux
  const middleware = [thunk];


  // Perform middleware call. Wrap in dev tools if necessary.
  let middlewareCall = applyMiddleware(...middleware);
  if (process.env.NODE_ENV !== "production") {
    middlewareCall = composeWithDevTools(middlewareCall);
  }

  // Create the Redux store and inject middlewares
  const store = createStore(
    rootReducer,
    middlewareCall
  );

  return store;
}