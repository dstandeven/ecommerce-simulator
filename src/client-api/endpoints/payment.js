import { mapResponse, methods, request, getPaymentURL } from "../index";

export const postPayment = async (body, mapper = mapResponse) => {
  const response = await request({
    route: `${getPaymentURL()}`,
    method: methods.POST,
    authorize: false,
    body
  });

  return mapper(response);
}