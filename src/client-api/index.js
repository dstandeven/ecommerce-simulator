let paymentURL = undefined;
let authorizationToken = undefined;

// Initializer for the API
export const initialize = (payment) => {
  paymentURL = payment;
}

export const setAuthorizationToken = token => {
  authorizationToken = token;
}

export const getPaymentURL = () => paymentURL;

// API error
export class RequestError extends Error {
  constructor(message, status) {
    super(message);
    this.status = status;
  }
}

// HTTP methods
export const methods = {
  GET: "get",
  POST: "post",
  PUT: "put",
  DELETE: "delete"
};

// Maps all objects of a property
const mapObject = (source) => {
  // Create the object to map to
  let target = {};

  // Rename and copy each property
  for (let sourceName in source) {
    // Override for ID's
    let targetName = undefined;
    if (sourceName === sourceName.toUpperCase()) {
      targetName = sourceName.toLowerCase();
    } else {
      targetName = sourceName.charAt(0).toLowerCase() + sourceName.substring(1);
    }

    // Copy the property value
    target[targetName] = source[sourceName];
  }

  return target;
}

// Default mapper for responses
export const mapResponse = (response) => {
  // Nothing
  if (!response) {
    return response;
  }

  // Array
  if (Array.isArray(response)) {
    return response.map(item => mapObject(item));
  }

  // Single object
  if (typeof response === "object") {
    return mapObject(response);
  }

  // Simple type
  return response;
}

// Executes an HTTP request
export const request = async ({ route, method, authorize, body }) => {
  // Build request with properties that are always there
  let request = {
    method: method,
    headers: {
      "content-type": "application/json"
    },
    body: body ? JSON.stringify(body) : undefined
  };

  // Add authorization header if the token is present
  if (authorize) {
    request.headers["authorization"] = authorizationToken;
  }

  // Add no-cache headers if it is a GET request.
  if (method === methods.GET) {
    request.headers["cache-control"] = "no-cache"; // HTTP 1.1 no-cache implementation.
    request.headers["pragma"] = "no-cache"; // HTTP 1.0 -- no-cache implementation.
  }

  // Process request
  const response = await fetch(route, request);

  // Good
  if (response.ok) {
    return response.json();
  }

  // Read the message
  const responseBody = await response.json();

  // Read the message
  const message = responseBody && responseBody.Message ? response.Message : "Request Failed";
  throw new RequestError(message, response.status);
};