import React from "react";
import styled from "styled-components"

const StyledButton = styled.button`
  position: relative;
  min-width: 70px;
  margin: .4em 3px;
  padding: 10px;
  border-radius: 8px;
  border: 0;
  background-color: ${props => props.theme.blue};
  color: white;
  white-space: nowrap;
  text-decoration: none;
  cursor: pointer;
  font-size: 1.1em;
  box-shadow: 0;

  &:focus {
      outline: 0;
      outline-offset: -4px;
  }

  &:active {
      transform: scale(0.99);
  }

  &.disabled {
    cursor: default;
    opacity: .5;
  }

  &.hoverable:hover {
    opacity: 1;
  }
`

// Button component
const Button = ({ disabled, text, onClick }) => {
  // Disabled switch
  let clickHandler = onClick;
  let className = "";
  if (disabled || !onClick) {
    clickHandler = () => { };
    className += "disabled";
  } else {
    className += "hoverable";
  }

  return (
    <StyledButton 
      className={className} 
      onClick={(event) => {
        event.preventDefault();
        clickHandler()
      }}
    >
      {text}
    </StyledButton>
  );
}

export default Button;