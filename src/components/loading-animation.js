import React from "react";
import styled, { keyframes } from "styled-components";

const Container = styled.div`
  display: flex;
  flex: 1;
  min-width: 0;
  min-height: 0;
  justify-content: center;
  text-align: center;
  text-align-last: center;
  align-items: center;
  vertical-align: middle;
`;

const KeyFrames = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 30px;
  border: 6px solid transparent;
  border-top-color: rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  @media only screen and (min-width: 769px) {
    width: 160px;
    height: 160px;
    border-radius: 150px;
    border: 15px solid transparent;
    border-top-color: rgba(0, 0, 0, 0.3);
  }
  animation: ${KeyFrames} 1.2s linear infinite;
  -webkit-animation: ${KeyFrames} 1.2s linear infinite;
`;

const LoadingAnimation = () => (
  <Container>
    <LoadingIcon />
  </Container>
)

export default LoadingAnimation;